let data = require("./arrays-people-friends.js")
  
  //1. Make the balance into a number. If the number is invalid, default to 0. Do not overwrite the existing key, create a new one instead.
  
  function dataWithConvertedBalance(data) {
    let dataWithConvertedBalance = data.map((each, index) => {
      let convertedBalance = Number(each.balance.replace("$", "").replace(",", ""))
      each.convertedBalance = convertedBalance
      return each
    }).map((each, index) => {
      if (isNaN(each.convertedBalance)) {
        each.convertedBalance = 0
      }
      return each
    })
    return dataWithConvertedBalance
  }
  
  let newDataWithConvertedBalance = dataWithConvertedBalance(data)
  console.log(newDataWithConvertedBalance)
  console.log('\n')
  
  
  //2. Sort by descending order of age.
  function dataSortByAge(data) {
    let dataSortByAge = data.sort((firstValue, secondValue) => {
      if (firstValue.age < secondValue.age) return 1
      else if (firstValue.age > secondValue.age) return -1
      else return 0
    })
    return dataSortByAge
  }
  let newDataBySortingAge = dataSortByAge(data)
  console.log(newDataBySortingAge)
  console.log('\n')
  
  
  //3. Flatten the friends key into a basic array of names.
  
  function flattenFriendsKey(data) {
    let flattenFriendsKey = data.map((each, index) => {
      let friends = each.friends.map((eachFriend, index) => {
        return eachFriend.name
      })
      each.friends = friends
      return each
    })
    return flattenFriendsKey
  }
  let dataWithUpdatedFriends = flattenFriendsKey(data)
  console.log(dataWithUpdatedFriends)
  console.log("\n")
  
  
  //4. Remove the tags key.
  
  function removeTags(data) {
    let newDataWithoutTags = newDataBySortingAge.map(each => {
      delete each.tags
      return each
    })
    return newDataWithoutTags
  }
  newDataWithoutTags = removeTags(data)
  console.log(newDataWithoutTags)
  console.log('\n')
  
  
  /*5. Make the registered date into the DD/MM/YYYY format.*/
  
  function changeDateFormat(data){
    let newDateFormat = data.map((each,index) => {
      let date = each.registered.split("-")
      date = date[2].slice(0,2) + "/" + date[1] + "/" + date[0]
      each.registered = date
      return each
    })
    return newDateFormat
  }
  
  let newDataWithDateFormat = changeDateFormat(data)
  console.log(newDataWithDateFormat)
  console.log("\n")
  
  
  //6. Filter only the active users.
  
  function activeUsers(data) {
    let activeUsers = data.filter((each, index) => {
      return (each.isActive === true)
    }).map((eachActiveUser, index) => {
      return eachActiveUser
    })
    return activeUsers
  }
  
  let activeUsersData = activeUsers(data)
  console.log(activeUsersData)
  console.log('\n')
  
  
  //7. Calculate the total balance available.
  function totalBalance(data){
    let sum = data.map((each,index) => {
      let balance = Number(each.balance.replace("$", "").replace(",", ""))
      if (isNaN(balance)) return 0
      else return balance
    }).reduce((acc,curr) => {
      return acc + curr
    },0)
    return sum
  }
  
  let totalBalanceResult = totalBalance(data)
  console.log(totalBalanceResult)
  
  //Chain everything.*/
  